#include <cstdio>
#include <cstring>
using namespace std;
int n, m;
int a[100], b[100];

int main() {
	//1. 读入n和m
	scanf("%d%d", &n, &m); 

    //2. 把数组a和b初始化为0。共有2n的0.
	for(int i=0; i<n; ++i) 
		a[i] = b[i] = 0;   

    //3. 循环m次
	for(int i=1; i<=m; ++i) {
		//4. 读入两个整数x和y
		int x, y;
		scanf("%d%d", &x, &y);

		//5. 条件：老大说了算 - 对应的位置上如果已经放入过数字，新的数字必须大于之前的数字
		if(a[x] < y && b[y] < x) {
			//6. 预先处理：如果x或y上曾经放入过数字，要把上次和他们配对的数字先清掉
			if(a[x] > 0)
				b[a[x]] = 0;
			if(b[y] > 0) 
				a[b[y]] = 0;
			//7. 交叉放入相应的位置
			a[x] = y;
			b[y] = x;  //3, 2  a[3] = 2; b[2] = 3;
		}
	}

    //8. 统计最后还有几个0
	int ans = 0;
	for(int i=1; i<=n; ++i) {
		if(a[i] == 0 )
			++ans;
		if(b[i] == 0 )
			++ans;
	}
	printf("%d\n", ans);
	return 0;
}