#include <cstdio>
using namespace std;
const int maxn = 10000;
int n;
int a[maxn];
int b[maxn];
//传入左下标，右下标，和深度
int f(int l, int r, int depth) {
	//1. 退出条件，返回0
	if(l>f)
		return 0;

	//2. 找出最小的数字，和对应的下标
	int min = maxn, mink;
	for(int i=l; i<=r; ++i) {
		if(min > a[i]) {
			min = a[i];
			mink = i; //记录最小值的下标
		}
	}

	//3. 递归求左边的子数组
	int lres = f(l, mink-1, depth + 1);

	//4. 递归求右边的子数组
	int rres = f(mink+1, r, depth + 1);

	//5. 最后返回值，注意这里和depth有关，且用的是b数组
	return lres + rres + depth * b[mink];
}

int main() {
	cin >> n;
	for(int i=0; i<n; ++i)
		cin >> a[i];
	for(int i=0; i<n; ++i)
		cin >> b[i];
	cout << f(0, n-1, 1) << endl;
	return 0
}