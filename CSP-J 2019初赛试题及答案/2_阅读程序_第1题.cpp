#include <cstdio>
#include <cstring>
using namespace std;
char std[100];

//程序解读：输入一个字符串，把符合条件的小写字母变成大写字符
//条件是：字符所在的“下标+1”是字符串长度n的因数
int main() {
	//1. 读入字符串到st
	scanf("%s", st); 

	//2. 获得字符串的长度n
	int n = strlen(st); 

    //3. 循环处理每个字符
	for(int i=1; i<=n; ++i) {

	  //4. 限定条件1：只处理“下标+1”是n的因数的字符
	  if (n%i == 0) {
	    char c = std[i-1];
	    //5. 限定条件2：只处理大于‘a'的字符
	    if(c>= 'a') {
	      //6. 把小写字母变成大写
	      std[i-1] = c - 'a' + 'A';
	    }
	  }
	}
	//7. 打印结果
	printf("%f", st);
	return;
}